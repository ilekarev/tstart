const { src, dest, watch, parallel, series } = require('gulp');
const rename = require('gulp-rename');

let isEnvDev = false;

const path = {
    src: './src',
    dest: './dest',
}
path.data = {
    src: path.src + '/_data',
    filename: 'data.json',
};
path.styles = {
    src: path.src + '/styles',
    dest: path.dest + '/styles',
};
path.scripts = {
    src: path.src + '/scripts',
    dest: path.dest + '/scripts',
};
path.fonts = {
    src: path.src + '/fonts',
    filename: '**/*',
    dest: path.dest + '/fonts',
};
path.images = {
    src: path.src + '/images',
    filename: '**/*.{jpg|png|webp}',
    dest: path.dest + '/images',
};
path.favicon = {
    src: path.src,
    filename: 'favicon.ico',
    dest: path.dest,
};
path.htaccess = {
    src: path.src,
    dest: path.dest,
    filename: '.htaccess.config',
};
path.html = {
    src: path.src,
    dest: path.dest,
};
path.templates = {
    src: path.src + '/_templates',
    dest: path.src,
};

function copy(key) {
    return src(path[key].src + '/' + path[key].filename).pipe(dest(path[key].dest));
}
function fonts() {
    return copy('fonts');
}
function images() {
    return copy('images');
}
function favicon() {
    return copy('favicon');
}
function htaccess() {
    return src(path.htaccess.src + '/' + path.htaccess.filename)
        .pipe(rename('.htaccess'))
        .pipe(dest(path.htaccess.dest));
}

function styles() {
    const postcss = require('gulp-postcss');
    const easyImport = require('postcss-easy-import');
    const postcssPresetEnv = require('postcss-preset-env');
    const autoprefixer = require('autoprefixer');
    const cssnano = require('cssnano');

    return src([path.styles.src + '/**/*.css', '!' + path.styles.src + '/**/_*.css'])
        .pipe(postcss([
            easyImport(),
            postcssPresetEnv(),
            autoprefixer(),
            cssnano(),
        ]))
        .pipe(dest(path.styles.dest));
}

function scripts(done) {
    const glob = require('glob');
    const browserify = require('browserify');
    const babelify = require("babelify");
    const source = require('vinyl-source-stream');

    return glob(path.scripts.src + '/*.js', function (error, files) {
        if (error) {
            return done(error);
        }

        const promises = [];
        const presets = ['@babel/preset-env'];
        const destination = isEnvDev ? path.scripts.src : path.scripts.dest;
        if (!isEnvDev) {
            presets.push('minify');
        }
        files.forEach(function (file) {
            promises.push(new Promise( (resolve) => {
                browserify({ entries: [file] })
                    .transform(babelify.configure({
                        presets: presets,
                    }))
                    .bundle()
                    .pipe(source(file))
                    .pipe(rename({
                        dirname: '',
                        extname: '.min.js',
                    }))
                    .pipe(dest(destination))
                    .on('finish', resolve);
            }));
        });

        return Promise.all(promises).then(done);
    });
}
function scriptsDev(done) {
    isEnvDev = true;
    return scripts(done);
}

function clean() {
    const del = require('del');

    return del([
        path.dest,
        path.templates.dest + '/**/*.html',
        path.scripts.src + '/*.min.js',
    ], { force: true });
}

function render() {
    const fs = require('fs');
    const nunjucks = require('nunjucks');
    const markdown = require('nunjucks-markdown');
    const marked = require('marked');
    const gulpNunjucks = require('gulp-nunjucks');

    const env = new nunjucks.Environment(new nunjucks.FileSystemLoader(path.templates.src));
    marked.setOptions({
        "headerIds": false,
    });
    markdown.register(env, marked);

    return src(path.templates.src + '/pages/**/*.njk')
        .pipe(rename({extname: '.html'}))
        .pipe(gulpNunjucks
            .compile(
                JSON.parse(fs.readFileSync(path.data.src + '/' + path.data.filename, 'utf8')),
                {
                    env: env,
                }
            )
        )
        .pipe(dest(path.templates.dest));
}

function html() {
    const htmlmin = require('gulp-htmlmin');

    return src(path.html.src + '/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(dest(path.html.dest));
}

function serve() {
    const browserSync = require('browser-sync');
    const reload = browserSync.reload;

    clean();
    render();
    scriptsDev();

    browserSync.init({
        server: path.src,
    });

    watch([path.data.src + '/' + path.data.filename, path.templates.src]).on('change', render);
    watch([path.scripts.src + '/**/*.js', '!' + path.scripts.src + '/*.min.js']).on('change', scriptsDev);
    watch([path.html.src + '/**/*.html', path.styles.src + '/**/*.css', path.scripts.src + '/*.min.js']).on('change', reload);
}

const build = series(clean, render, parallel(styles, scripts, html, htaccess, fonts, images, favicon));

exports.build = build;
exports.serve = serve;
exports.clean = clean;

exports.default = build;
